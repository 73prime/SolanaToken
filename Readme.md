# What is this?

In short, it's a:
* VM setup for things
* Further instructions on how to create your own token on the Solana blockchain

### Installation Steps & Prereqs

* Download the [Phantom wallet](https://phantom.app/download), install in your browser.

* Get your Linux machine stood up. (Use the `vagrant up` command, or else some other way).

* Update things (if not using vagrant box with it pre-installed)
```
sudo apt update
```

* Install other tools (if not using vagrant box with it pre-installed)
```
sudo apt install libudev-dev
sudo apt install libssl-dev pkg-config
sudo apt install build-essential
```

* Install Solana CLI (if not using vagrant box with it pre-installed)
```
sh -c "$(curl -sSfL https://release.solana.com/v1.8.5/install)"
```
* Exit terminal, log back in (for PATH settings to apply).

* Install Rust
```
curl https://sh.rustup.rs -sSf | sh
```
Choose default installation

* Exit, log back into terminal for PATH

* Install solana's token program
```
cargo install spl-token-cli
```

## Create Token

* Create wallet
```
solana-keygen new
```
Take note of public Key. This will how you seed your Solana. Take note of passphrase, and save it.

* Check balance:
```
solana balance
```
* Go buy Solana ($10), send it to the public wallet address.

* Check balance:
```
solana balance
```

* Create token
```
spl-token create-token
```
Take note of the token address. (no need on the signature)
* Check balance
```
solana balance
```
* Create an account to hold the wallet
```
spl-token create-account <token address>
```
This will take solana; this will cost a bit more.
Take note of the wallet address
* Check balance
```
solana balance
```
* Mint tokens
```
spl-token mint <token address> <number to mint> <token account public address just made>
```
This will cost solana
* Check balance
```
solana balance
```
* Check wallet 
```
spl-token accounts
```
* Send it to your phantom wallet:
```
spl-token transfer --fund-recipient --allow-unfunded-recipient <token address> <number to send> <wallet address for phantom>
```
"fund-recipient" auto-creates the token account in the solana wallet. 

* Check Phantom wallet.

* Check solscan. [Solscan](https://solscan.io). Copy token address, and view it on the blockchain.

* To mint more, simply do the `spl-token mint` command again.

## Configuring Token

* Have logo ready, < 200Kb, store it somewhere publicly available: S3, Github, etc.

* Use Github account. Login, and fork the [Solana Labs Token List repo](https://github.com/solana-labs/token-list)

* Make two changes: 
    - Add folder named <token address>, in the `assets` folder, with logo committed underneath (named logo.png)
    - Make entry in the `src/tokens/solana.tokenlist.json` at the end, with the token information.

* Create a PR on the Solana Labs Token List repo, and watch for it to merge.





